#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# Please read hook.sh
# This script empties out the "Thin" server's nginx config after a
# session was terminated.

THIN_IP=$(cat /share/wg_thin_ip)

ssh thin@"$THIN_IP" truncate -s 0 /usr/local/etc/nginx/zTu.conf
ssh thin@"$THIN_IP" sudo service nginx reload 2> >(grep -v gethostbyname)
