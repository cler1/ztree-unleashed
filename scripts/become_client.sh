#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

echo "The following clients are available:$(tput sgr0)"
echo

./available_clients.sh

echo
echo "$(tput setaf 6)Which client do you want to become?"
tput sgr0
read -r user

sudo su - "${user}"
