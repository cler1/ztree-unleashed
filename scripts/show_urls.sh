#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

./generate_urls.sh > /share/.session/urls

if [ -z "${DISPLAY}" ]
then
	echo "Here are your URLs. Note that the first URL is the"
	echo "experimenter's URL; $(tput bold)DO NOT DISTRIBUTE IT.$(tput sgr0)"
	tput setaf 6
	cat /share/.session/urls
	tput sgr0
	echo
else
    {
        echo "EXPERIMENTER URL -- DO NOT DISTRIBUTE --"
        head -n 1 /share/.session/urls
        echo "SUBJECT URLS -- PLEASE DISTRIBUTE --"
        tail -n +2 /share/.session/urls
    } > /share/.session/urls_show
	
	geany /share/.session/urls_show >/dev/null 2>&1
fi
