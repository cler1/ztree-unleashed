#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

# usage: ./start_zleaf.sh user display core fontsize channel

if [[ $# -lt 5 || $# -gt 5 ]]
then
	echo "ERROR: Invalid number of arguments to start_zleaf.sh. Exiting."
	exit 1
fi

if [ -f ~/.zu/language ]; then
	ZTREEFLAGS="/language $(cat ~/.zu/language)"
else
	ZTREEFLAGS=""
fi

if [ -f ~/.zu/rtf_enabled ]; then
	STARTFLAGS="WINEDLLOVERRIDES=riched20=n"
else
	STARTFLAGS=""
fi

if [ -f /share/preleaf_linux ]; then
	./as_another_tmux.sh "$1" "DISPLAY=:$2 /share/preleaf_linux; DISPLAY=:$2 ${STARTFLAGS} /usr/bin/taskset -c $3 /usr/bin/wine /share/.session/zleaf.exe ${ZTREEFLAGS} /channel $5 /size $(cat /share/geometry) /name $1 /fontsize $4"
	./as_another_tmux.sh "$1" "sleep 5; DISPLAY=:$2 xdotool windowmove \\\$(DISPLAY=:$2 xdotool search --name Experiment) 60 60"
elif [ -f /share/preleaf.exe ]; then
	./as_another_tmux.sh "$1" "DISPLAY=:$2 ${STARTFLAGS} /usr/bin/taskset -c $3 /usr/bin/wine /share/preleaf.exe 'Z:/share/.session/zleaf.exe' '${ZTREEFLAGS} /channel $5 /size $(cat /share/geometry) /name $1 /fontsize $4'"
elif [ -f /share/vorschaltung.exe ]; then
	./as_another_tmux.sh "$1" "DISPLAY=:$2 ${STARTFLAGS} /usr/bin/taskset -c $3 /usr/bin/wine /share/vorschaltung.exe 'Z:/share/.session/zleaf.exe' '${ZTREEFLAGS} /channel $5 /size $(cat /share/geometry) /name $1 /fontsize $4'"
else
	./as_another_tmux.sh "$1" "DISPLAY=:$2 ${STARTFLAGS} /usr/bin/taskset -c $3 /usr/bin/wine /share/.session/zleaf.exe ${ZTREEFLAGS} /channel $5 /size $(cat /share/geometry) /name $1 /fontsize $4"
fi

# source for the taskset cmd: Oliver Kirchkamp
# https://www.kirchkamp.de/lab/zTree.html
# Thanks!
