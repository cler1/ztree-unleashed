#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

echo "$(tput setaf 2)z-Tree unleashed"
echo

if [ -f /share/install ]; then
 	echo "File '/share/install' exists. It seems you have already" 1>&2
 	echo "initialized z-Tree unleashed. Therefore I refuse to init" 1>&2
 	echo "a second time. If you want to reinitialize, delete users" 1>&2
 	echo "using ./delete_users.sh and remove /share/install, then" 1>&2
 	echo "re-run this script. $(tput setaf 5)" 1>&2
 	echo "Did you perhaps only want to change the 'Settings'? (./settings.sh)" 1>&2
 	exit 1
fi

if [[ ! -f /etc/zu_oem && $(whoami) != "lab" ]]; then
	echo "You are not running the VM, but you are not running under" 1>&2
	echo "the user 'lab'. This is not allowed. I refuse to continue." 1>&2
	exit 1
fi

if [ -f /etc/zu_oem ]; then
	echo "$(tput setaf 3)Please set a new password for the user 'lab'. You will need"
	echo "$(tput setaf 3)to use this password with SSH, FileZilla, the login, etc."
	echo "$(tput setaf 3)$(tput bold)You will NOT see anything as you type your password:$(tput sgr0)"
	echo
	sudo passwd lab
	echo
	
	sudo rm -f /etc/ssh/ssh_host_*
	sudo ssh-keygen -A > /dev/null
	sudo systemctl restart ssh.service
else
	echo "$(tput setaf 3)During the execution of this program, you may be"
	echo "$(tput setaf 3)prompted to enter the superuser (root) password."
fi

sudo groupadd clients 2>/dev/null
sudo usermod -a -G clients lab 2>/dev/null

sudo rm -rf /share/.session
sudo rm -f ~/share

sudo mkdir -p /share/zTree
sudo mkdir -p /share/scratch
sudo mkdir -p /share/.session

sudo ln -s /share ~/share
sudo chown -R lab:clients /share
sudo chmod -R 777 /share/scratch

./settings.sh

source read_settings.sh

if [ ! -f ~/.ssh/id_ed25519 ]; then
	mkdir -p ~/.ssh
	ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -q -N ''
fi

echo 95 > ~/.zu/nclients

if [[ ! -f /etc/zu_oem_users ]]; then
	echo
	echo "$(tput setaf 3)We will now create 95 clients, which is the current"
	echo "$(tput setaf 3)maximum for one session, plus a ${PREFIX}0 for you,"
	echo "$(tput setaf 3)the honorable experimenter. The clients will be"
	echo "$(tput setaf 3)called ${PREFIX}1, ${PREFIX}2, ..., ${PREFIX}95."
	echo "$(tput setaf 3)This process might take a few minutes."
	echo
	echo "$(tput setaf 5)Please press enter to continue.$(tput sgr0)"
	echo
	read -r

	cp ~/ztree-unleashed/extras/antialiasing/antialiasing.reg /tmp

    for i in $(seq 0 95)
	do
		sudo useradd "${PREFIX}${i}" -s "$(command -v bash)" -m -g users -G clients
		echo "${PREFIX}${i}:$(./genpw.sh 32)" | sudo chpasswd
		sudo mkdir -p "/home/${PREFIX}${i}/.zu"
		sudo chown -R "${PREFIX}${i}":clients "/home/${PREFIX}${i}"
		sudo chmod 700 "/home/${PREFIX}${i}"
		./as_another_tmux.sh "${PREFIX}${i}" "wine regedit /tmp/antialiasing.reg"

		echo -n "$(tput setaf 6)User ${PREFIX}${i} created. $(tput sgr0)"
		sleep 5
	done
fi

echo

touch /share/install
touch ~/.zu/new_nginx_path
./extract_icons.sh

while pgrep wineboot > /dev/null; do
	sleep 1
done

sleep 1

echo
echo "$(tput setaf 5)Initialization complete."
echo "$(tput setaf 5)Remember to cite z-Tree unleashed if you use it"
echo "$(tput setaf 5)in your research:$(tput setaf 2)"
echo
fold -s -w 60 < citation.txt
echo
echo "$(tput setaf 5)Thank you,"
echo "$(tput setaf 5)Matthias, Max and Thomas$(tput sgr0)"
