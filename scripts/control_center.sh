#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

if [[ -f /etc/zu_oem ]]; then
	if sudo pgrep Xvfb > /dev/null; then
		echo "Please wait..."

		dpkg -s python3-tk > /dev/null 2>&1 || {
			echo "Installing dependencies (python3-tk)..."
			sudo apt install -y python3-tk > /dev/null 2>&1
		}

		dpkg -s python3-pip > /dev/null 2>&1 || {
		    echo "Installing dependencies (python3-pip)..."
			sudo apt install -y python3-pip > /dev/null 2>&1
        }

		python3 -c "import watchdog" > /dev/null 2>&1 || {
			echo "Installing dependencies (pip3:watchdog)..."
			pip3 install watchdog > /dev/null 2>&1
        }

		tmux new-session -d ./control_center.py
		exit 0
	else
		echo "It seems you are not running a session. Please start a"
		echo "session before using the Control Center."
		
		exit 2
	fi
else
	echo "You are not running the OEM version of z-Tree unleashed."
	echo "Please run ./control_center.py manually, ensuring that"
	echo "all dependencies (python3-tk, python3-pip, pip3:watchdog)"
	echo "are installed."
	
	exit 1
fi
