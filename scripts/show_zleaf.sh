#!/usr/bin/env bash

#  This Source Code Form is subject to the terms of the z-Tree unleashed
#  License, v. 1.0. If a copy of the zTuL was not distributed with this
#  file, You can obtain one by emailing zTuL@max.pm.

source read_settings.sh

echo "$(tput setaf 2)z-Tree unleashed"
echo "Show z-Leaf"
echo

echo "The following clients are available:$(tput sgr0)"
echo

./available_clients.sh

echo
echo "$(tput setaf 6)Which client do you want to view?"
echo "$(tput setaf 6)NOTE: This is VIEW-ONLY. You cannot interact"
echo "with the screen directly."
tput sgr0
read -r user

VNCPORT=$(grep -wE "${user}" /share/.session/sessionfile | awk '{print $3}')

vncviewer -shared -viewonly -autopass "127.0.0.1::${VNCPORT}" < ~/.zu/viewpasswd >/dev/null 2>&1
