# z-Tree unleashed

## Introduction

*z-Tree unleashed* enables you to run your [z-Tree](https://www.ztree.uzh.ch/en.html) experiments over the Internet. Subjects can participate from anywhere in the world.

*z-Tree unleashed* is permissively licensed under a MPL-2.0-style license. The only additional requirement is that you cite the following paper if you use *z-Tree unleashed* in an academic context:

> Duch, M. L., Grossmann, M. R. P. and Lauer, T. (2020). "z-Tree unleashed:
> A novel client-integrating architecture for conducting z-Tree experiments
> over the Internet". Working Paper Series in Economics No. 99, University
> of Cologne.

Please read our [working paper](https://www.researchgate.net/publication/340273792_z-Tree_unleashed_A_novel_client-integrating_architecture_for_conducting_z-Tree_experiments_over_the_Internet)! It's sorta awesome.

*z-Tree unleashed* is a set of scripts that should run on most POSIX compatible x86 or x64 platforms.

We offer two main methods of installing *z-Tree unleashed*:

1. Via our virtual machine image: You can simply download our virtual machine image and import it into [VirtualBox](https://www.virtualbox.org/). After some simple steps (see below), you're all set.

2. Using your own server: We offer detailed step-by-step instructions in our documentation.

## Next steps
Please refer to our [documentation](https://cler1.gitlab.io/ztree-unleashed-doc/docs/installation/).
